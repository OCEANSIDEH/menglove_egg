module.exports = options => {
	return async function jwtErr(ctx, next) {
		// 获取请求头的token值
		const headerStr = ctx.request.header.authorization;
		// 如果有值
		if (headerStr) {
			try {
				// 解码token，需传加密时的 secret
				const decode = ctx.app.jwt.verify(headerStr, options.secret);
				ctx.state.result = decode; // 信息存一下，这步很重要，业务里要用
				await next();  //执行完才执行下一步
				
			} catch (error) { //失败执行
				ctx.status = 401;
				// 翻译错误码
				let message = error.message;
				ctx.body = {
					code: -1,
					msg: message,
				};
				return;
			}
		} else {  //如果没有token
			ctx.status = 401;
			ctx.body = {
				code: -1,
				msg: '没有token值',
			};
			return;
		}
	};
};
