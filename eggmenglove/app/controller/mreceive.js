'use strict';

const Controller = require('./core/base_controller');

/**
 * @Controller 萌领
 */
class petsController extends Controller {
     
     /**
     * @summary 萌领列表
     * @description Pet List
     * @router get /petsList
     * @response 200 petsList ok
     */
    
    // 获取宠物的全部列表
	async petsList() {
		const {
			ctx
		} = this;
		const data = await ctx.service.mreceive.petsList('petsList');
		if(data){
		    this.success(data);
		}else{
		    this.fail();
		}
	}
	
	/**
     * @summary 宠物详情
     * @description Pet Detail
     * @router get /petDetails
     * @request query String id 宠物id
     * @response 200 petsList ok
     */
    
    // 获取宠物的全部列表
	async petDetails() {
		const {
			ctx
		} = this;
        let petId = ctx.query.id;
		const data = await ctx.service.mreceive.petDetails(petId,'petsList');
		if(data){
		    this.success(data);
		}else{
		    this.fail();
		}
		
	}
	
	/**
	 * @summary 寻宠列表
	 * @description Find Pet List
	 * @router get /findPetsList
	 * @response 200 petsList ok
	 */
	
	async findPetsList() {
		const {
			ctx
		} = this;
		const data = await ctx.service.mreceive.petsList('findPets');
		if(data){
		    this.success(data);
		}else{
		    this.fail();
		}
	}
	/**
	 * @summary 寻宠详情
	 * @description Find Pet Detail
	 * @router get /findPetsDetails
	 * @request query String id 宠物id
	 * @response 200 petsList ok
	 */
	async findPetsDetails() {
		const {
			ctx
		} = this;
	    let petId = ctx.query.id;
		const data = await ctx.service.mreceive.petDetails(petId,'findPets');
		console.log(333,{data});
		if(data){
		    this.success(data);
		}else{
		    this.fail();
		}
	}
	
	/**
	 * @summary 寻宠发布
	 * @description Pet seeking release
	 * @router post /petsRelease
	 * @request body petsRelease
	 */
	
	// 	发布丢失宠物
	async petsRelease() {
		const {
			ctx
		} = this;
		const userId = ctx.state.user.userId //获取解密的token的用户Id
		let releaseData = ctx.request.body; //post请求获取的数据
		const {type,data} = await ctx.service.mreceive.petsRelease(userId,releaseData,'findPets');
		if(type=='fail'){
		    this.fail('提交失败');
		}else{
		    this.lackFail(data);
		}
	}
	
	/**
	 * @summary 寻主列表
	 * @description Find Pet List
	 * @router get /findMastersList
	 * @response 200 petsList ok
	 */
	
	async findMastersList() {
		const {
			ctx
		} = this;
		const data = await ctx.service.mreceive.petsList('findMasters');
		if(data){
		    this.success(data);
		}else{
		    this.fail();
		}
	}
	/**
	 * @summary 寻主详情
	 * @description Find Masters Detail
	 * @router get /findMastersDetails
	 * @request query String id 宠物id
	 * @response 200 petsList ok
	 */
	async findMastersDetails() {
		const {
			ctx
		} = this;
	    let petId = ctx.query.id;
		const data = await ctx.service.mreceive.petDetails(petId,'findMasters');
		if(data){
		    this.success(data);
		}else{
		    this.fail();
		}
	}
	
	/**
	 * @summary 寻主发布
	 * @description Pet seeking release
	 * @router post /mastersRelease
	 * @request body petsRelease
	 */
	
	// 	发布找到的宠物
	async mastersRelease() {
		const {
			ctx
		} = this;
		const userId = ctx.state.user.userId //获取解密的token的用户Id
		let releaseData = ctx.request.body; //post请求获取的数据
		const {type,data} = await ctx.service.mreceive.petsRelease(userId,releaseData,'findMasters');
		if(type=='fail'){
		    this.fail('提交失败');
		}else{
		    this.lackFail(data);
		}
	}
	
}

module.exports = petsController;

