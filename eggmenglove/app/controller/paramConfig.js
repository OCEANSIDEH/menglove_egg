'use strict';

const Controller = require('./core/base_controller');

class ParamConfigController extends Controller {
	async paramConfig() {
		const {
			ctx
		} = this;
		let paramKey = ctx.query.paramKey;
		const data = await ctx.service.paramConfig.paramConfig(paramKey);
		if (data) {
			this.success(data);
		} else {
			this.fail();
		}
	}
}

module.exports =  ParamConfigController;