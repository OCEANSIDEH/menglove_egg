const Controller = require('./core/base_controller');

/**
 * @Controller 用户信息
 */
class userController extends Controller {
    /**
     * @summary 获取用户信息
     * @description user info
     * @router get /userInfo
     * @response 200 userInfo ok 
     */
	async userInfo() {
		const {
			ctx,
			app
		} = this;
		const userId = ctx.state.user.userId //获取解密的token的用户Id
		console.log("ctx.state.user",ctx.state.user);
		console.log("ctx.state",ctx.status);
		let type = ctx.query.type; //获取请求的type类型
		// app.use(jwtErr)
		console.log(userId);
		if(type!='all'){
			const result = await this.app.mysql.get('userinfo', {
				userId
			})
			if (result) {  //请求成功
				ctx.body = {
					code: 200,
					message: '成功',
					data: result, //将用户信息返回
					// userId: userId
				}
			} else {
				ctx.body = {
					code: 500,
					message: '失败',
					data: result, //将用户信息返回
					// userId: userId
				}
			}
		}else{
			// 获取全部用户信息
			const result = await this.app.mysql.select('userinfo')
			if (result) {  //请求成功
				ctx.body = {
					code: 200,
					message: '成功',
					data: result, //将用户信息返回
					// userId: userId
				}
			} else {
				ctx.body = {
					code: 500,
					message: '失败',
					data: result, //将用户信息返回
					// userId: userId
				}
			}
			// ctx.body = {
			// 	code: 500,
			// 	message: '失败',
			// 	// data: result, //将用户信息返回
			// 	// userId: userId
			// }
		}
		//查询解密的userId的用户信息
		// console.log(userId);
		// // console.log("result", result)
		// console.log("ctx.status", ctx.status);
		// // const token = ctx.request.header.authorization;
		// console.log("requestToken", token);
		// console.log("token", ctx.token);
		
		
	}
	
	/**
     * @summary 修改用户信息
     * @description edit user info
     * @router post /editUserInfo
     * @request body userInfo
     */
    //修改用户信息
    async editUserInfo() {
        const {
			ctx,
			app
		} = this;
		const userId = ctx.state.user.userId; //获取解密的token的用户Id
		let releaseData = ctx.request.body;
        const data = await ctx.service.user.petsList(userId,releaseData);
        if(data){
		    this.success(data);
		}else{
		    this.fail();
		}
    }
	

}

module.exports = userController;
