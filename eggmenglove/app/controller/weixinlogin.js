const Controller = require('egg').Controller;

class WeixinLoginController extends Controller {
	async weixin() {
		const {ctx}=this
		let user=await ctx.request.body;
		console.log('user',user)
		const access_token = `${user.access_token}`; // 前端用户微信token标识
		const openid = `${user.openid}`;  //id相当于用户名
		// 请求外部接口获取用户信息（请求的数据都是二进制Buffer格式）
		const info= await ctx.curl("https://api.weixin.qq.com/sns/userinfo?access_token="+access_token+"&openid="+openid);
		const userinfo=JSON.parse(JSON.stringify(info))  //将请求的数据先转换为json格式
		const aa=new Buffer(userinfo.data.data).toString()  //转换为二进制再转换为字符串
		const infos=JSON.parse(aa)//再将数据转化为json格式
		const username=openid;  //赋值（用于数据查询）
		const password=access_token;
		console.log("user_info",userinfo)
		const usertable = await this.app.mysql.get('user', {
			username
		});  //向user数据表查找openid数据
		if(usertable){  //查到该数据直接登录（存token）并更新用户信息（userinfo）数据
			// 存储token
			const token = this.app.jwt.sign({
				userId: usertable.id, //需要存储的Token数据(存token的标识是用户的id) 
				// usertable是查询该条数据的内容，将该id赋值给userId,解密需要
			}, this.app.config.jwt.secret, {
				expiresIn: '3000s', //设置有效时间
			});
			console.log(token);
			ctx.session.user = usertable;
			ctx.body = {
				code: 0,
				message: '登录成功',
				token: token, //获取token /将生成的Token返回给前端
			}
		}else { //将数据存进去再登录（存token）
		// 获取用户信息
			const result = await this.app.mysql.insert('user', {
				username: openid,
				password: '',
				repassword: '',
				phone: '',
			}); //向user里面插入username和password的值
			if (result.affectedRows === 1) { //affectedRows=1 插入数据后成功后返回的
				this.app.mysql.insert('userinfo', {
					userId: result.insertId,
					nickname: infos.nickname,  //用户名
					phone: '',
					rate: 0,
					headimg:infos.headimgurl, //头像
				}); //向用户信息添加id和用户名（登录成功后可获取）
				// 存储token
				const token = this.app.jwt.sign({
					userId: result.insertId, //需要存储的Token数据(存token的标识是用户的id) 
					// usertable是查询该条数据的内容，将该id赋值给userId,解密需要
				}, this.app.config.jwt.secret, {
					expiresIn: '3000s', //设置有效时间
				});
				console.log(token);
				ctx.session.user = result.insertId;
				ctx.body = {
					code: 0,
					message: '登录成功',
					token: token, //获取token /将生成的Token返回给前端
				}
			} else {
				this.ctx.body = { //注册失败返回
					code: 1,
					message: '登录失败',
				}
			}
		}
	}
}

module.exports = WeixinLoginController;
