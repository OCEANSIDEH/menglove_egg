
/* egg_folder/app/controller/book.js */
'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller 登陆
 */
class LoginController extends Controller {
     
     /**
     * @summary 登陆接口
     * @description Login
     * @router get /index
     * @request query string username 用户名
     * @request query string password 用户密码
     * @response 200 login ok
     */

	async index() {
		const {
			ctx
		} = this;
		let user = ctx.query; //get请求
		const username = user.username;
		const password = user.password;
		// const result=await app.mysql.get('user',{username:username,password:password});
		const result = await this.app.mysql.get('user', {
			username,
			password
		});
		// console.log('result',result.id);
		if (result) { //如果登录输入的账号密码查询成功（有值）
			const token = this.app.jwt.sign({
				userId: result.id, //需要存储的Token数据(存token的标识是用户的id) 
				 // result是查询该条数据的内容，将该id赋值给userId,解密需要
			}, this.app.config.jwt.secret, {
			   expiresIn: '3000s',  //设置有效时间
			});
			console.log(token);
			 ctx.session.user = result;
			ctx.body = {
				code: 0,
				message: '登录成功',
				token: token, //获取token /将生成的Token返回给前端
			}
		} else {
			ctx.body = {
				code: 1,
				message: '抱歉，登录失败',
			}
		}
	}
}

module.exports = LoginController;
