'use strict';

const Controller = require('./core/base_controller');

class HomeController extends Controller {
     /**
    * @summary 根据Id获取信息
    * @description 根据Id获取信息
    * @router get /v1/book/getBook
    * @request query integer Id ID
    * @response 200 JsonResult 操作结果
    */
	// 全部资讯
	async realTimeInfo() {
		const {
			ctx,
			app
		} = this;
		// 获取Service 层数据
		const data = await ctx.service.news.realTimeInfo();
		// 	返回数据	
		if (data) {
			this.success(data);
		} else {
			this.fail();
		}
	}

	// 单个资讯分类
	async newsTypeInfo() {
		const {
			ctx,
			app
		} = this;
		const typeid = ctx.query.id;
		const data = await ctx.service.news.realTimeInfo(typeid);
		if (data) {
			this.success(data);
		} else {
			this.fail();
		}
	}

	// 资讯详情
	async newsInfo() {
		const {
			ctx
		} = this;
		const newsId = ctx.query.id;
		const real_time_info = await ctx.service.news.findNewsInfo(newsId);
		// 	返回数据
		if (real_time_info) {
			this.success(real_time_info);
		} else {
			this.fail();
		}
	}
}

module.exports = HomeController;
