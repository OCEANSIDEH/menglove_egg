const Controller = require('egg').Controller;

class UserController extends Controller {
	async create() {
		const {
			ctx
		} = this; //ctx就是请求
		let user = ctx.request.body; //post请求获取的数据
		let userS = ctx.query //get请求获取的数据
		// const username = ` ${userS.username}`;  //将前端输入的数据获取赋值
		// const password = `${userS.password}`;
		const username = `${user.username}`; //将前端输入的数据获取赋值 (用户名)
		const password = `${user.password}`;  //密码
		const repassword = `${user.repassword}`;   //再次确认密码
		const phone=`${user.phone}`;  //手机号
		const usertable = await this.app.mysql.get('user', {
			username
		});  //向user数据表查找数据
		console.log("usertable", usertable);
		if (usertable) {  //如果在user表查找到数据就不插入数据
			this.ctx.body = {
				code: 2,
				message: '该账户已存在',
			}
		} else {
			const result =await this.app.mysql.insert('user', {
				username: username,  
				password: password,
				repassword: repassword,
				phone:phone,
			}); //向user里面插入username和password的值
			console.log(result);
			console.log("ruquest", user);
			console.log("ctx", ctx.request.body);
			if (result.affectedRows === 1) { //affectedRows=1 插入数据后成功后返回的
			this.app.mysql.insert('userinfo', {
				userId: result.insertId,
				nickname: user.username,
				phone:phone,
				rate:0,
			}); //向用户信息添加id和用户名（登录成功后可获取）
				this.ctx.body = {  //注册成功返回
					code: 0,
					message: '注册成功',
					// id: result.insertId, //插入的id
					// username: user.username,
					// password: user.password,
					// repassword: user.repassword,
					// phone:user.phone,
				}
				// 如果注册成功后获取用户id，把用户名添加进
			} else {
				this.ctx.body = {   //注册失败返回
					code: 1,
					message: '注册失败',
				}
			}
		}

	}
}

module.exports = UserController;
