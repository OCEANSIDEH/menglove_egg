const Controller = require('./core/base_controller');
//文件存储
const fs=require('fs');
const path=require('path');
const awaitWriteStream=require('await-stream-ready').write;
const senToWormhole=require('stream-wormhole');

/**
 * @Controller 上传文件
 */
class uploadController extends Controller {
    /**
     * @summary 上传图片
     * @description upload image
     * @router post /uploadImg
     * @request query String imageUrl 图片的文件地址
     * @response 200 uploadImg ok
     */
    // 保存头像/封面
    async uploadImg() {
        const { ctx, app } = this;
        const stream = await ctx.getFileStream();  //上传文件时获取文件信息
        const newTime = new Date().getTime();  //当前时间  
        let randomNumber = Math.random().toString(36).substr(2);   //随机数
        let fileBasePath = this.config.baseDir;  //上传根目录
        const filePath = 'app/public/upload';  //文件路径
        //文件名：随机数+时间戳+原文件后缀
        // path.extname(stream.filename).toLocaleLowerCase()为后缀名（.jpg,.png等）
        let fileName = randomNumber + newTime + path.extname(stream.filename).toLocaleLowerCase();
        // 同步读取文件
        let files = fs.readdirSync(filePath);//读取该文件夹
        //文件完整路径 存储图片文件夹
        const filePathName  = path.join(fileBasePath, filePath, fileName);
        let writerStream = fs.createWriteStream(filePathName);  //创建文件流
        stream.pipe(writerStream);  //将图片添加进项目文件中
        writerStream.on('error', function(err) {
           throw err.stack;
        });
       let imageUrl = 'public/upload/'+fileName;   // 前端显示 http://119.23.57.157:7001/ +imageUrl
       this.success({imageUrl:imageUrl});
    }

}

module.exports = uploadController;