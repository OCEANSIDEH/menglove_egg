const Controller = require('./core/base_controller');

class forumController extends Controller {
	async index() {
		const {
			ctx,
			app
		} = this;
		let id = ctx.query.id; 
		let res;
		if(id){
			res = await app.mysql.get('forum',{id});
			
		}else{
			res = await app.mysql.select('forum');
		}
		// const res = await app.mysql.select('forum');
		if (res) {
			ctx.body = {
				status: 200,
				data: res,
				message: '获取成功'
			}
		} else {
			ctx.body = {
				status: 500,
				message: '请求失败',
			}
		}
	}
	/**
	 * @summary 萌圈发布
	 * @description forum seeking release
	 * @router post /forumRelease
	 * @request body forumRelease
	 */

	// 	萌圈发布
	async forumRelease() {
		const {
			ctx
		} = this;
		const userId = ctx.state.user.userId //获取解密的token的用户Id
		let releaseData = ctx.request.body; //post请求获取的数据
		const {
			type,
			data
		} = await ctx.service.forum.forumRelease(userId, releaseData);
		if (type == 'fail') {
			this.fail('提交失败');
		} else {
			this.lackFail(data);
		}
	}
	// 	萌友详情
	async momentDetail() {
		const {
			ctx
		} = this;
		let userId = ctx.query.id;
		console.log({userId});
		const data = await ctx.service.forum.momentDetail(userId);
		console.log(222,{data});
		if (data) {
			this.success(data);
		} else {
			this.fail();
		}
	}
}

module.exports = forumController;
