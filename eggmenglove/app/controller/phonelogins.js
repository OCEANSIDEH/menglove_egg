const Controller = require('egg').Controller;

class UserController extends Controller {
	async create() {
		const {
			ctx
		} = this; //ctx就是请求
		let user = ctx.request.body; //post请求获取的数据
		let userS = ctx.query //get请求获取的数据
		console.log(userS);
		const phone = userS.phone
		console.log(phone);
		const usertable = await this.app.mysql.get('user', {
			phone
		});  //查找该手机号是否注册过
		console.log("phone", phone);
		if (usertable) { //如果在用户表里面查找到该手机号（直接登录）
			// 存储token
			const token = this.app.jwt.sign({
				userId: usertable.id, //需要存储的Token数据(存token的标识是用户的id) 
				// usertable是查询该条数据的内容，将该id赋值给userId,解密需要
			}, this.app.config.jwt.secret, {
				expiresIn: '3000s', //设置有效时间
			});
			console.log(token);
			ctx.session.user = usertable;
			ctx.body = {
				code: 0,
				message: '登录成功',
				token: token, //获取token /将生成的Token返回给前端
			}
		} else { //将数据存进去再登录（存token）
			const result = await this.app.mysql.insert('user', {
				username: phone,
				password: phone,
				repassword: phone,
				phone: phone,
			}); //向user里面插入username和password的值
			if (result.affectedRows === 1) { //affectedRows=1 插入数据后成功后返回的
				this.app.mysql.insert('userinfo', {
					userId: result.insertId,
					nickname: phone,
					phone: phone,
					rate: 0,
				}); //向用户信息添加id和用户名（登录成功后可获取）
				// 存储token
				const token = this.app.jwt.sign({
					userId: result.insertId, //需要存储的Token数据(存token的标识是用户的id) 
					// usertable是查询该条数据的内容，将该id赋值给userId,解密需要
				}, this.app.config.jwt.secret, {
					expiresIn: '3000s', //设置有效时间
				});
				console.log(token);
				ctx.session.user = result.insertId;
				ctx.body = {
					code: 0,
					message: '登录成功',
					token: token, //获取token /将生成的Token返回给前端
				}
			} else {
				this.ctx.body = { //注册失败返回
					code: 1,
					message: '注册失败',
				}
			}
		}

	}
}

module.exports = UserController;
