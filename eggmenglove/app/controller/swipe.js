'use strict';

const Controller = require('egg').Controller;

class SwipeController extends Controller {
	/**
	 * @summary authorize
	 * @description authorize
	 * @router POST /index
	 * @consumes html/text applicatoin/json
	 * @request body authorize_login *login 
	 * @response 200 baseResponse ok
	 */
	async index() {
		const {
			ctx,
			app
		} = this;
		// 查询数据表的全部数据（数据库是test，查询的是数据表tabletest）
		const res = await app.mysql.select('swipe');
		ctx.body = res
		// 查询表格id为1的数据
		// let result = await app.mysql.get("swipe",{id:1})
		// console.log(result)
		// ctx.body =result
		if (res) {
			ctx.body = {
				status: 200,
				data: res,
				message: '请求成功'
			};
		} else {
			ctx.body = {
				status: 500,
				message: '请求失败'
			};
		}
	}
}

module.exports = SwipeController;