const { Controller } = require('egg');
class BaseController extends Controller {
	// get user() {
	//   return this.ctx.session.user;
	// }

	// 请求成功
	success(data) {
		this.ctx.body = {
		    code: 200,
			success: true,
			data,
			message: "获取成功"
		};
	}
	
	// 请求失败
	fail(message){
		message = message || "获取失败";
		this.ctx.body = {
			code: 500,
			message: message,
		}
	}
	
	// 提交缺少数据
	lackFail(message){
		message = message || "请完整填写信息";
		this.ctx.body = {
			code: 200,
			message: message,
		}
	}

	// 没有找到
	notFound(message) {
		message = message || "not found";
		this.ctx.throw(404, message);
	}
}
module.exports = BaseController;
