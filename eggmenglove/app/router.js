'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller, jwt  } = app;  //声明变量赋值，不同的方法用不同，就不用都用app
  // router.get('/', controller.home.index);
  router.post('/register', controller.register.create);  //注册
  router.get('/login', controller.login.index);  //登录
  router.get('/phoneLogin', controller.phonelogins.create);  //手机号登录
  router.post('/weixinLogin', controller.weixinlogin.weixin);  //微信登录
  router.get('/swipe' ,controller.swipe.index);  //轮播图  //jwt 需要验证Token的路由
  router.get('/forum', jwt ,controller.forum.index);  // 论坛 萌圈
  router.post('/forumRelease', jwt ,controller.forum.forumRelease);  //发布萌圈
  router.get('/momentDetail', jwt ,controller.forum.momentDetail);  //萌友
  router.get('/realTimeInfo', controller.news.realTimeInfo); //资讯 全部
  router.get('/newsTypeInfo', controller.news.newsTypeInfo); //资讯 单个
  router.get('/newsInfo', controller.news.newsInfo); //资讯
  
  router.get('/petsList', controller.mreceive.petsList); //宠物列表
  router.get('/petDetails', controller.mreceive.petDetails); //宠物详情
  
  router.get('/findPetsList', controller.mreceive.findPetsList); //寻宠列表
  router.get('/findPetsDetails', controller.mreceive.findPetsDetails); //寻宠详情
  router.post('/petsRelease',jwt, controller.mreceive.petsRelease); //寻宠发布
	
  router.get('/findMastersList', controller.mreceive.findMastersList); //寻主列表
  router.get('/findMastersDetails', controller.mreceive.findMastersDetails); //寻主详情
  router.post('/mastersRelease',jwt, controller.mreceive.mastersRelease); //寻主发布
  

  router.get('/userInfo',jwt,controller.userInfo.userInfo);// 用户信息
  router.post('/editUserInfo',jwt,controller.userInfo.editUserInfo);// 用户信息修改
  
  router.post('/uploadImg',controller.upload.uploadImg);// 上传图片
  
  router.get('/paramConfig',controller.paramConfig.paramConfig);// 配置信息
  
  router.get('/', controller.test.test);
};
