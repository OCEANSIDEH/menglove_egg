const Service = require('egg').Service;

class ConfigService extends Service {
	 // 萌友
	 async paramConfig(paramKey) {
	 	const {
	 		app
	 	} = this;
	 	let configItem = await app.mysql.get('param_config', {
	 		paramKey
	 	})
	 	// const paramConfigList = await app.mysql.select(
	 	// 	'param_config', {
	 	// 		where: {
	 	// 			paramKey
	 	// 		}, // 这里是你的查询条件
	 	// 		// columns: ['id', 'name', 'email'] // 指定返回的字段
	 	// 	});
	 	// configItem.paramConfigList = paramConfigList;
	 	return configItem;
	 }
}
module.exports = ConfigService;