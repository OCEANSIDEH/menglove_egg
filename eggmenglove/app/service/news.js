const Service = require('egg').Service;

// app/controller/news.js
class NewsService extends Service {
     /**
     * 根据id主键查询数据
     * @param {*} id 
     */
	// 获取全部资讯并分类
	async realTimeInfo(typeid) {
		const {
			app
		} = this;
		// 查询数据表的全部数据
		const news_info_type = await app.mysql.select('news_info_types'); //资讯分类
		const real_time_info = await app.mysql.select('real_time_info'); //资讯基本信息
		/**
		 * 构建树形结构数据
		 * @param {*} data
		 */
		// 添加根节点
		const res = [];
		for (let item of news_info_type) {
			// 找出所有根节点
			if (item.type) {
				item.children = []; //可以有多个
				for (let itemChild of real_time_info) {
					// 找出所有子节点
					if (item.type == itemChild.typeId) {
						//   传入根节点id 递归查找所有子节点
						item.children.push(itemChild);
						//   item.children = itemChild;
					}
				}
				res.push(item);
			}
		}
		if (typeid) { //单个的分类
			for (let typeitem of res) {
				if (typeid == typeitem.id) {
					return typeitem;
				}
			}
		} else { //全部的分类
			return res;
		}
	}
	// 获取资讯详情
	async findNewsInfo(newsid) {
		// 从数据库获取资讯的详细信息
		const real_time_info = await this.app.mysql.get('real_time_info', {
			id: newsid
		});
		return {
			real_time_info
		};
	}

}
module.exports = NewsService;
