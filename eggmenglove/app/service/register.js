const Service = require('egg').Service;
class UserService extends Service {
    async create(user)  {
        let {app}=this;
        let  result = await app.mysql.insert('user',
             user
           );
        return  result;
     }

}
module.exports = UserService;