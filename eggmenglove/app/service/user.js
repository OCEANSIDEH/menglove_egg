const Service = require('egg').Service;

class UserService extends Service {
    	// 宠物列表
	async petsList(userId,releaseData) {
		const {
			app
		} = this;
// 		const userinfoList = await app.mysql.select('userinfo'); //全部用户信息
        const userInfo = await app.mysql.get('userinfo',userId)
        // const headimg = releaseData.headimg;
		const row = {
		    headimg:releaseData.headimg || userInfo.headimg,
		    nickname:releaseData.nickname || userInfo.nickname,
		    phone:releaseData.phone || userInfo.phone
		}
		const options = {
		     where: {
		         userId: userId
             }
        };
        const result = await app.mysql.update('userinfo', row,options);
        // => UPDATE `userinfo` SET `headimg` = headimg,  WHERE userId = userId ;
		if(result.affectedRows === 1){
		  //  const userinfoList = await app.mysql.select('userinfo'); //全部用户信息
		    const result = await app.mysql.get('userinfo',userId)
		    return result;
		}else{
		    return null;
		}
	}
}

module.exports = UserService;
