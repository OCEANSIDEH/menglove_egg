const Service = require('egg').Service;
const Date = require("../extend/date.js"); //导入Date的模块

class PetsService extends Service {

	// 宠物列表
	async petsList(type) {
		const {
			app
		} = this;
		const petsList = {
			'petsList': await app.mysql.select('mreceive'), //全部宠物
			'findPets': await app.mysql.select('find_pets'), //寻宠列表
			'findMasters': await app.mysql.select('find_masters'), //寻主列表
		}
		return petsList[type];
	}
	// 宠物详情
	async petDetails(id, type) {
		const {
			app
		} = this;
		const petItem = {
			'petsList': await app.mysql.get('mreceive', {
				id
			}), //全部宠物详情
			'findPets': await app.mysql.get('find_pets', {
				id
			}), //寻宠详情
			'findMasters': await app.mysql.get('find_masters', {
				id
			}), //寻主详情
		}
		return petItem[type];
	}

	// 	发布宠物
	async petsRelease(userId, releaseData,type) {
		const {
			app
		} = this;
		const userInfo = await app.mysql.get('userinfo', userId)
		const userResult = {
			publisher: userInfo.nickname,
			publisherImg: userInfo.headimg || 'public/upload/headimg.jpg',
			publisherTime: Date.currentFormatDate('month'),
			cuteHeart: userInfo.rate,
		};
		// const petsRelease = await app.mysql.select('find_masters'); //发布宠物列表
		let errorInfo = function(mes) {
			return {
				type: 'error',
				data: mes
			};
		}
		
		if(!releaseData.name)  return errorInfo('请填写宠物名称');
		if(!releaseData.petsImg)  return errorInfo('请填写内容图片');
		if(!releaseData.contactsPhone)  return errorInfo('请填写联系人手机号');
		if(!releaseData.reasonContent)  return errorInfo('请填写送养原因');
		if(!releaseData.varieties)  return errorInfo('请填写品种');
		if(!releaseData.age)  return errorInfo('请填写宠物年龄');
		if(!releaseData.address)  return errorInfo('请填写宠物所在地');
		if(!releaseData.time)  return errorInfo('请填写走失发现时间');
		// return {type:'error',data:{...releaseData,...userResult}};
		
		// 已填写全部信息
		const data = { ...releaseData, ...userResult};
		let result;
		if(type=='findPets'){
			result = await app.mysql.insert('find_pets', data);
		}else{
			result = await app.mysql.insert('find_masters', data);
		}
		// const result = await app.mysql.insert('mreceive', data); //将请求的数据添加进寻找宠物列表
		//  const petsRelease =await app.mysql.select('mreceive'); //发布宠物列表
		if (result.affectedRows === 1) { // 插入数据后成功后返回的
			return {
				type: 'success',
				data: '提交成功'
			};
		} else {
			return {
				type: 'fail',
				data: 'null'
			};
		}
	}


}
module.exports = PetsService;
