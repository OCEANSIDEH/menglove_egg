const Service = require('egg').Service;
const Date = require("../extend/date.js"); //导入Date的模块

class ForumService extends Service {
	// 	发布萌圈
	async forumRelease(userId, releaseData) {
		const {
			app
		} = this;
		const userInfo = await app.mysql.get('userinfo', userId)
		const userResult = {
			publisher: userInfo.nickname,
			publisherImg: userInfo.headimg || 'public/upload/headimg.jpg',
			time: Date.currentFormatDate('month'),
			cuteHeart: userInfo.rate,
			address: userInfo.address,
			userId
		};
		let errorInfo = function(mes) {
			return {
				type: 'error',
				data: mes
			};
		}

		if (!releaseData.content) return errorInfo('请输入发布的内容');
		if (!releaseData.image) return errorInfo('请上传图片图片');
		// 已填写全部信息
		const data = {
			...releaseData,
			...userResult
		};
		let result = await app.mysql.insert('forum', data)
		console.log({
			result
		});
		if (result.affectedRows === 1) { // 插入数据后成功后返回的
			return {
				type: 'success',
				data: '提交成功'
			};
		} else {
			return {
				type: 'fail',
				data: 'null'
			};
		}
	}
	// 萌友
	async momentDetail(userId) {
		const {
			app
		} = this;
		let userItem = await app.mysql.get('userinfo', {
			userId
		})
		const forumList = await app.mysql.select(
			'forum', {
				where: {
					userId
				}, // 这里是你的查询条件
				// columns: ['id', 'name', 'email'] // 指定返回的字段
			});
		userItem.forumList = forumList;
		return userItem;
	}


}
module.exports = ForumService;
