// app/contract/response/base.js
'use strict';

module.exports = {
    // 测试模块
    testResponse: {
        message: { type: 'string' }
    },
    // 登陆
    login: {
      username: {type:'string', required:true, description:'登录账号',example:'zuweie'},
      password: {type:'string', required:true, description:'登录密码', example:'123456'},
    },
     // 寻宠列表展示
    petsList: {
      id: {type:'string', required:true, description:'宠物id',example:'1'},
      name: {type:'string', required:true, description:'宠物名字',example:'中华田园猫'},
      petsImg: {type:'string', required:true, description:'内容图片',example:'https://i.loli.net/2021/12/01/zYcHwZB9pCyRn6j.jpg,https://i.loli.net/2021/12/01/zYcHwZB9pCyRn6j.jpg'},
      publisherTime: {type:'string', required:true, description:'发布时间',example:'2021-02-19'},
      publisherAddress: {type:'string', required:true, description:'发布地址',example:'湖南省宜章县'},
      publisher: {type:'string', required:true, description:'发布者',example:'OCEANSIDE'},
      publisherImg: {type:'string', required:true, description:'发布者头像',example:'https://i.loli.net/2021/12/01/zYcHwZB9pCyRn6j.jpg'},
      cuteHeart: {type:'string', required:true, description:'萌心值',example:'2'},
      contactsPhone: {type:'string', required:true, description:'联系人手机号',example:'15999930763'},
      reasonContent: {type:'string', required:true, description:'送养原因 （或者寻主寻宠的宠物特征）',example:'猫咪内敛、优雅、自负、忠于自己的感觉。。。。。'},
      isInsectRepellent: {type:'string', required:true, description:'是否驱虫 (mysql数据库的布尔型对应的数据类型为tinyint(1)，存入的数据，0代表false，1代表true)领养的才需要',example:'1'},
      isSterilization: {type:'string', required:true, description:'是否绝育 领养的才需要',example:'0'},
      isImmune: {type:'string', required:true, description:'是否免疫 领养的才需要',example:'1'},
      isPhysicalExamination: {type:'string', required:true, description:'是否体检 领养的才需要',example:'null'},
      varieties: {type:'string', required:true, description:'品种',example:'中华田园猫'},
      gender: {type:'string', required:true, description:'性别 0母 1公',example:'0'},
      age: {type:'string', required:true, description:'年龄',example:'3-4个月'},
      isAdopt: {type:'string', required:true, description:'是否领养 (0 未领养 1领养)',example:'1'},
      address: {type:'string', required:true, description:'宠物所在地',example:'湖南省宜章县'},
      time: {type:'string', required:true, description:'走失发现时间 (领养可以没有)',example:'2022-01-15'},
    },
    // 发布寻宠
    petsRelease: {
      name: {type:'string', required:true, description:'宠物名字',example:'中华田园猫'},
      petsImg: {type:'string', required:true, description:'内容图片',example:'https://i.loli.net/2021/12/01/zYcHwZB9pCyRn6j.jpg,https://i.loli.net/2021/12/01/zYcHwZB9pCyRn6j.jpg'},
      contactsPhone: {type:'string', required:true, description:'联系人手机号',example:'15999930763'},
      reasonContent: {type:'string', required:true, description:'送养原因 （或者寻主寻宠的宠物特征）',example:'猫咪内敛、优雅、自负、忠于自己的感觉。。。。。'},
      isInsectRepellent: {type:'string', required:true, description:'是否驱虫 (mysql数据库的布尔型对应的数据类型为tinyint(1)，存入的数据，0代表false，1代表true)',example:'1'},
      isSterilization: {type:'string', required:true, description:'是否绝育',example:'0'},
      isImmune: {type:'string', required:true, description:'是否免疫',example:'1'},
      isPhysicalExamination: {type:'string', required:true, description:'是否体检',example:'null'},
      varieties: {type:'string', required:true, description:'品种',example:'中华田园猫'},
      gender: {type:'string', required:true, description:'性别 0母 1公',example:'0'},
      age: {type:'string', required:true, description:'年龄',example:'3-4个月'},
      isAdopt: {type:'string', required:true, description:'是否领养 (0 未领养 1领养)',example:'1'},
      address: {type:'string', required:true, description:'宠物所在地',example:'湖南省宜章县'},
	  detailAddress: {type:'string', required:true, description:'宠物所在详细地址',example:'里田乡'},
      time: {type:'string', required:true, description:'走失发现时间 (领养可以没有)',example:'2022-01-15'},
    },
    // 上传图片
    uploadImg: {
      uploadImg: {type:'string', required:true, description:'图片本地地址 （使用时加上请求的域名 eg:http://119.23.57.157:7001/）',example:'public/upload/ahqmbd59xd1644807432745.jpeg'},
    },
    // 用户信息
    userInfo: {
      userId: {type:'string', required:false, description:'用户id',example:'1'},
      nickname: {type:'string', required:true, description:'用户昵称',example:'OCEANSIDE'},
      phone: {type:'string', required:true, description:'手机号',example:'15220802681'},
      headimg: {type:'string', required:true, description:'头像',example:'public/upload/ahqmbd59xd1644807432745.jpeg'},
      rate: {type:'string', required:false, description:'萌心值',example:'4'},
    },
};