/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
	const path = require('path');
	/**
	 * built-in config
	 * @type {Egg.EggAppConfig}
	 **/
	const config = exports = {};
	// 跨域问题
	config.security = {
		csrf: {
			enable: false,
			ignoreJSON: true
		},
		domainWhiteList: ['*'] //[]中放放出的白名单，*代表所有
	};
	config.cors = {
		origin: '*',
		allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH',
	};
	config.jwt = {
		secret: '123456', //自定义token的加密条件字符串，可按各自的需求填写
	};
	// sequelize 配置
	config.sequelize = {
		dialect: 'mysql',
		host: '127.0.0.1',
		port: 3306,
		username: 'root',
		password: 'root',
		database: 'menglove_mysql',
		define: {
			underscored: true, // 注意需要加上这个， egg-sequelize只是简单的使用Object.assign对配置和默认配置做了merge, 如果不加这个 update_at会被转变成 updateAt故报错
			// 禁止修改表名，默认情况下，sequelize将自动将所有传递的模型名称（define的第一个参数）转换为复数
			// 但是为了安全着想，复数的转换可能会发生变化，所以禁止该行为
			freezeTableName: true
		}
	};
	exports.mysql = {
		// 单数据库信息配置
		client: {
			// host
			host: 'localhost',
			// 端口号
			port: '3306',
			// 用户名
			user: 'root',
			// 密码
			password: 'root',
			// 数据库名
			database: 'menglove_mysql',
		},
		// 是否加载到 app 上，默认开启
		app: true,
		// 是否加载到 agent 上，默认关闭
		agent: false,
	};

	// use for cookie sign key, should change to your own and keep security
	config.keys = appInfo.name + '_1608545357059_451';

	// add your middleware config here
	config.middleware = [];

	// swagger配置
// 	config.static = {
// 		prefix: '/public/',
// 		dir: path.join(appInfo.baseDir, '/app/public/'),
// 		apis: './router.js',

// 	};
	
	
	/**
 * egg-swagger-doc default config
 * @member Config#swagger-doc
 * @property {String} dirScanner - 插件扫描的文档路径
 * @property {String} basePath - api前置路由
 * @property {Object} apiInfo - 可参考Swagger文档中的Info
 * @property {Array[String]} apiInfo - 可参考Swagger文档中的Info
 * @property {Array[String]} schemes - 访问地址协议http或者https
 * @property {Array[String]} consumes - contentType的集合
 * @property {Array[String]} produces - contentType的集合
 * @property {Object} securityDefinitions - 安全验证，具体参考swagger官方文档
 * @property {Boolean} enableSecurity - 是否使用安全验证
 * @property {Boolean} routeMap - 是否自动生成route
 * @property {Boolean} enable - swagger-ui是否可以访问
 */
	exports.swaggerdoc = {
    dirScanner: './app/controller', // 配置自动扫描的控制器路径
    // 接口文档的标题，描述或其它
    apiInfo: {
        title: 'mengLove-api',  // 接口文档的标题
        description: 'swagger-ui for Render document.',   // 接口文档描述
        version: '1.0.0',   // 接口文档版本
    },
    schemes: ['http', 'https'], // 配置支持的协议
    consumes: ['application/json'], // 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html
    produces: ['application/json'], // 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回
    securityDefinitions: {  // 配置接口安全授权方式
        // apikey: {
        //   type: 'apiKey',
        //   name: 'clientkey',
        //   in: 'header',
        // },
        // oauth2: {
        //   type: 'oauth2',
        //   tokenUrl: 'http://petstore.swagger.io/oauth/dialog',
        //   flow: 'password',
        //   scopes: {
        //     'write:access_token': 'write access_token',
        //     'read:access_token': 'read access_token',
        //   },
        // },
    },
    enableSecurity: false,  // 是否启用授权，默认 false（不启用）
    // enableValidate: true,    // 是否启用参数校验，默认 true（启用）
    routerMap: true,    // 是否启用自动生成路由，默认 true (启用)
    enable: true,   // 默认 true (启用)
};
	// config.static = {
	//     prefix:'/public/',
	//     dir: path.join(appInfo.baseDir, 'app/public'),
	// }

	// 执行的顺序   jwtCheck token的配置（配置了才能执行中间件）
	// config.middleware = ['jwtErr'];
	// exports.middleware = ['jwtErr']; // 数组的顺序为中间件执行的顺序

// 上传文件存储位置
config.uploadDir = 'app/public/admin/upload';
	// add your user config here
	const userConfig = {
		// myAppName: 'egg',
	};

	return {
		...config,
		...userConfig,
	};



};
