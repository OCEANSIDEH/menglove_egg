'use strict';

/** @type Egg.EggPlugin */
module.exports = {
	// 数据库插件
	mysql: {
		enable: true,
		package: 'egg-mysql',
	},
	// 跨域问题
	cors: {
		enable: true,
		package: 'egg-cors',
	},
	// 跨域身份验证
	jwt: {
		enable: true,
		package: 'egg-jwt',
	},
	// 配置 egg-swagger-doc 插件信息
    swaggerdoc: {
        enable: true,   // 启用 swagger-ui 默认启用
        package: 'egg-swagger-doc', // 指定 第三方插件 包名称
    },
	// 数据源框架
// 	sequelize: {
// 		enable: true,
// 		package: 'egg-sequelize',
// 	}

};
